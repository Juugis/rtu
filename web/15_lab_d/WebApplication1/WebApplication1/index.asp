﻿<%
    Dim colors(3)
    colors(0)="Black"
    colors(1)="Red"
    colors(2)="Green"
    colors(3)="Blue"
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        function changeColor(obj,txt){
            txt = document.getElementById(txt);
            txt.style.color = obj.value;
        }
    </script>
</head>

<body>
    <h3>Colors:</h3>
    <select id="colors" onchange="changeColor(this,'text')">
        <% For Each x In colors %>
           <option value="<% =x %>"><% =x %></option> 
        <% Next %>
    </select>

    <h1 id="text">Active Server Pages</h1>
</body>
</html>