6 variants
1, 2c, 3b, 4a

1) paredzēt iespēju lietotājam aizpildīt bināro meklēšanas koku (BMK) atbilstoši variantiem ar veselām virsotnēm
(kamēr koks nav izveidots, citas funkcijas vai nu ir nepieejamas vai, izvēloties kādu no tām, lietotājam tiek
izvadīts paziņojums, ka koks nav izveidots);
2) izvadīt uz ekrāna binārā meklēšanas koka virsotnes, izmantojot vienu no apgaitas algoritmiem:
    c) inorderālais.

3) atrast iegūtā binārā meklēšanas kokā un izvadīt rezultātus ekrānā:
    b) cik kokā ir virsotnes, kurām ir viens kreisais bērns;
    
4) izpildīt ar koku šādas darbības un izvadīt rezultātus ekrānā:
    a) atrast minimālo un maksimālo elementu;
