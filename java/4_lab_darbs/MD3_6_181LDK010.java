import java.util.*;

public class MD3_6_181LDK010
{
    private static void printEmptyBST(){
        System.out.println("Binārais koks ir tukš, Lūdzu aizpildiet bināro koku!");
    }

    public static void main(String args[]) {

        // Informācija par autoru
        System.out.println("\nAutors: Jurģis Brūvers");
        System.out.println("Apliecības Nr: 181LDK010");
        System.out.println("Kurss: 3. Koledža RDKD0");
        System.out.println("MD 3: 6. variants");

        Scanner scan = new Scanner(System.in);

        /* Creating object of class arrayStack */
        BST bst = new BST();
        char ch;

        /*  Perform list operations  */
        do{
            System.out.println("\nDarbības ar bināro koku:");
            System.out.println("1. insert ");
            System.out.println("2. delete");
            System.out.println("3. search");
            System.out.println("4. Bināro koka mezglu skaits?"); 
            System.out.println("5. pārbaudīt vai koks ir tukš?"); 
            System.out.println("6. Izvadīt bināro koku ar metodi: inorderālais?");
            System.out.println("7. cik kokā ir virsotnes, kurām ir viens kreisais bērns?");
            System.out.println("8. atrast minimālo un maksimālo elementu?"); 


            /* testešanas vērtības
            bst.insert( 35 );
            bst.insert( 17 );
            bst.insert( 20 );
            bst.insert( 14 );
            bst.insert( 9 );
            bst.insert( 3 );
            */


            int choice = scan.nextInt();
            switch (choice){
                case 1 :
                    System.out.println("Ievadiet vesalu skaitli:");
                    bst.insert( scan.nextInt() );
                    break;
                case 2 :
                    if(bst.isEmpty()){
                        printEmptyBST();
                    }else{
                        System.out.println("Ievadiet elementa skaitli:");
                        bst.delete( scan.nextInt() );
                    }
                    break;
                case 3 :
                    if(bst.isEmpty()){
                        printEmptyBST();
                    }else{
                        System.out.println("Lai atrastu elementa, ievadiet vesalu skaitli:");
                        System.out.println("Meklēšanas rezultāts : "+ bst.search( scan.nextInt() ));
                    }
                    break;
                case 4 :
                    if(bst.isEmpty()){
                        printEmptyBST();
                    }else{
                        System.out.println("Skaits = "+ bst.countNodes());
                    }
                    break;
                case 5 : 
                    if(bst.isEmpty()){
                        System.out.println("Koks ir tukš!");
                    }else{
                        System.out.println("Koks nav tukš!");
                    }
                    break; 
                case 6 : 
                    if(bst.isEmpty()){
                        printEmptyBST();
                    }else{
                        System.out.println("\n Inordeāls binārais koks: ");
                        bst.inorder();
                    }
                    break;
                case 7 : 
                    if(bst.isEmpty()){
                        printEmptyBST();
                    }else{
                        // cik kokā ir virsotnes, kurām ir viens kreisais bērns
                        System.out.println("\n Skaits: "+bst.countLeftNodes());
                    }
                    break;
                case 8 : 
                    if(bst.isEmpty()){
                        printEmptyBST();
                    }else{
                        System.out.println("\n Max Elementa vērtība: "+bst.getMaxElement());
                        System.out.println("\n Min Elementa vērtība: "+bst.getMinElement());
                    }
                    break;
                default : 
                    System.out.println("Opcija: "+choice+" neeksistē! \n ");
                    break;   
            }

            System.out.println("\nTurpināt? (y vai n) \n");
            ch = scan.next().charAt(0);                        
        } while (ch == 'Y'|| ch == 'y');   
        scan.close();
   }
}
     

/* Class BSTNode */
class BSTNode{
    BSTNode left, right;
    int data;


    public void print() {
        print("", this, false);
    }

    public void print(String prefix, BSTNode n, boolean isLeft) {
        if (n != null) {
            System.out.println (prefix + (isLeft ? "|-- " : "\\-- ") + n.data);
            print(prefix + (isLeft ? "|   " : "    "), n.left, true);
            print(prefix + (isLeft ? "|   " : "    "), n.right, false);
        }
    }

    /* Constructor */
    public BSTNode(){
        left = null;
        right = null;
        data = 0;
    }

    /* Constructor */
    public BSTNode(int n){
        left = null;
        right = null;
        data = n;
    }

    /* Function to set left node */
    public void setLeft(BSTNode n){
        left = n;
    }

    /* Function to set right node */ 
    public void setRight(BSTNode n){
        right = n;
    }

    /* Function to get left node */
    public BSTNode getLeft(){
        return left;
    }

    /* Function to get right node */
    public BSTNode getRight(){
        return right;
    }

    /* Function to set data to node */
    public void setData(int d){
        data = d;
    }

    /* Function to get data from node */
    public int getData(){
        return data;
    }
}

    

/* Class BST */
class BST
{
    private BSTNode root;

    /* print tree */
    public void print(){
        root.print();
    }

    /* Constructor */
    public BST(){
        root = null;
    }

    /* Function to check if tree is empty */
    public boolean isEmpty(){
        return root == null;
    }

    /* Functions to insert data */
    public void insert(int data){
        root = insert(root, data);
    }

    /* Function to insert data recursively */
    private BSTNode insert(BSTNode node, int data){
        if (node == null)
            node = new BSTNode(data);

        else{
            if (data <= node.getData())
                node.left = insert(node.left, data);

            else
                node.right = insert(node.right, data);
        }
        return node;
    }

    /* Functions to delete data */
    public void delete(int k){
        if (isEmpty())
            System.out.println("Tree Empty");

        else if (search(k) == false)
            System.out.println("Sorry "+ k +" is not present");

        else{
            root = delete(root, k);
            System.out.println(k+ " deleted from the tree");
        }
    }

    private BSTNode delete(BSTNode root, int k){
        BSTNode p, p2, n;
        if (root.getData() == k){

            BSTNode lt, rt;
            lt = root.getLeft();
            rt = root.getRight();

            if (lt == null && rt == null)
                return null;

            else if (lt == null){
                p = rt;
                return p;
            }

            else if (rt == null){
                p = lt;
                return p;
            }

            else{
                p2 = rt;
                p = rt;

                while (p.getLeft() != null)
                    p = p.getLeft();

                p.setLeft(lt);
                return p2;
            }
        }

        if (k < root.getData()){
            n = delete(root.getLeft(), k);
            root.setLeft(n);
        }

        else{
            n = delete(root.getRight(), k);
            root.setRight(n);
        }

        return root;
    }

    public int getMinElement() {
        return minElement(root).getData();
    }

    public int getMaxElement() {
        return maxElement(root).getData();
    }

    public static BSTNode minElement(BSTNode root) {
        if (root.left == null)
            return root;
        else {
            return minElement(root.left);
        }
    }

    public static BSTNode maxElement(BSTNode root) {
        if (root.right == null)
            return root;
        else {
            return maxElement(root.right);
        }
    }

    /* Functions to count number of nodes */
    public int countNodes(){
        return countNodes(root);
    }

    /* Function to count number of nodes recursively */
    private int countNodes(BSTNode r){
        if (r == null)
            return 0;
        else{
            int l = 1;
            l += countNodes(r.getLeft());
            l += countNodes(r.getRight());
            return l;
        }
    }

    public int countLeftNodes(){
        return countLeftNodes(root);
    }

    /* Function to count number of nodes recursively */
    private int countLeftNodes(BSTNode r){
        if (r == null)
            return 0;
        else{
            int l = 0;
            l += countLeftNodes(r.getLeft());
            l += countLeftNodes(r.getRight());
            if((r.getRight() == null) & (r.getLeft() != null)){
                l += 1;
            }
            return l;
        }

        // cik kokā ir virsotnes, kurām ir viens kreisais bērns
    }

    /* Functions to search for an element */
    public boolean search(int val){
        return search(root, val);
    }

    /* Function to search for an element recursively */
    private boolean search(BSTNode r, int val){
        boolean found = false;
        while ((r != null) && !found){
            int rval = r.getData();
            if (val < rval)
                r = r.getLeft();

            else if (val > rval)
                r = r.getRight();

            else{
                found = true;
                break;
            }

            found = search(r, val);
        }

        return found;
    }

    /* Function for inorder traversal */
    public void inorder(){
        inorder(root);
    }

    private void inorder(BSTNode r){
        if (r != null){
            inorder(r.getLeft());
            System.out.print(r.getData() +" ");
            inorder(r.getRight());
        }
    }

    /* Function for preorder traversal */
    public void preorder(){
        preorder(root);
    }

    private void preorder(BSTNode r){
        if (r != null){
            System.out.print(r.getData() +" ");
            preorder(r.getLeft());
            preorder(r.getRight());
        }
    }

    /* Function for postorder traversal */
    public void postorder(){
        postorder(root);
    }

    private void postorder(BSTNode r){
        if (r != null){
            postorder(r.getLeft());
            postorder(r.getRight());
            System.out.print(r.getData() +" ");
        }
    }
}