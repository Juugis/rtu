import java.util.*;

public class MD2_5_181LDK010
{
    public static void main(String args[]) {

        // Informācija par autoru
        System.out.println("\nAutors: Jurģis Brūvers");
        System.out.println("Apliecības Nr: 181LDK010");
        System.out.println("Kurss: 3. Koledža RDKD0");
        System.out.println("MD 2: 5. variants");

        Scanner scan = new Scanner(System.in);

        /* Creating object of linkedList */
        System.out.println("Ieavdiet steka izmēru: ");
        int n = scan.nextInt();
        /* Creating object of class arrayStack */
        arrayStack stk = new arrayStack(n);

        char ch;

        /*  Perform list operations  */
        do{
            System.out.println("\nDarbības ar setku:");
            System.out.println("1. push");
            System.out.println("2. pop");
            System.out.println("3. peek");
            System.out.println("4. Vai steks ir tukš?");
            System.out.println("5. Vai steks ir pilns?");
            System.out.println("6. Steka izmērs");
            System.out.println("7. cik nullu ir stekā?");
        
            int choice = scan.nextInt();

            switch (choice){
                case 1 :
                    System.out.println("Ievadiet veseli skaitli:");
                    try{
                        stk.push( scan.nextInt() );
                    }catch (Exception e){
                        System.out.println("Error : " + e.getMessage());
                    }
                    break;
                case 2 :
                    try{
                        System.out.println("Elements: " + stk.pop()+" tika noņemts!");
                    }catch (Exception e)
                    {
                        System.out.println("Error : " + e.getMessage());
                    }
                    break;
                case 3 :
                    try{
                        System.out.println("Virsotnes elements: " + stk.peek());
                    }catch (Exception e){
                        System.out.println("Error : " + e.getMessage());
                    }
                    break;
                case 4 :
                    if(stk.isEmpty()){
                        System.out.println("Steks IR tukš!");
                    }else{
                        System.out.println("Steks NAV tukš!");
                    }
                    break;
                case 5 :
                    if(stk.isFull()){
                        System.out.println("Steks IR pilns!");
                    }else{
                        System.out.println("Steks NAV pilns!");
                    }
                    break;
                case 6 : 
                    System.out.println("Steka izmērs: " + stk.getSize());
                    break;
                case 7 : 
                    System.out.println("Stekā atrodas: " + stk.findNullCount()+" nulles");
                    break;              
                default : 
                    System.out.println("Opcija: "+choice+" neeksistē! \n ");
                    break;   
            }

            stk.display();
            System.out.println("\nTurpināt? (y vai n) \n");
            ch = scan.next().charAt(0);                        
        } while (ch == 'Y'|| ch == 'y');   
        scan.close();
   }
}
     

/*  Class arrayStack  */
class arrayStack
{
    protected int arr[];
    protected int top, size, len;

    /*  Constructor for arrayStack */
    public arrayStack(int n){
        size = n;
        len = 0;
        arr = new int[size];
        top = -1;
    }

    /*  Function to check if stack is empty */
    public boolean isEmpty(){
        return top == -1;
    }

    /*  Function to check if stack is full */
    public boolean isFull(){
        return top == size -1 ;
    }

    /*  Function to get the size of the stack */
    public int getSize(){
        return len ;
    }

    /*  Function to check the top element of the stack */
    public int peek(){
        if( isEmpty() )
            throw new NoSuchElementException("Steks ir tukš");

        return arr[top];
    }

    /*  Function to add an element to the stack */
    public void push(int i){
        if(top + 1 >= size)
            throw new IndexOutOfBoundsException("Stekā vairs nav vietas");

        if(top + 1 < size )
            arr[++top] = i;
        len++ ;
    }

    /*  Function to delete an element from the stack */
    public int pop(){
        if( isEmpty() )
            throw new NoSuchElementException("Steks ir tukš");

        len-- ;
        return arr[top--]; 
    }

    /*  Function to delete an element from the stack */
    public int findNullCount(){
        int count = 0;

        for (int i = top; i >= 0; i--)
            if(arr[i] == 0)
                count++;
        
        return count; 
    }

    /*  Function to display the status of the stack */
    public void display(){
        System.out.println("\nSteka elementi:");

        if (len == 0){
            System.out.print("Steks ir tukš!\n");
            return ;
        }

        for (int i = top; i >= 0; i--)
            System.out.print(arr[i]+" ");
        System.out.println();
    }
}