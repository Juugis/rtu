Pusstatiskā datu struktūra: v-5  - 25.11.2020
5
Vektoriālā formā attēlots steks
1, 2a, 3a, 4, 5a

Uzdevums: Izstrādāt algoritmu un uzrakstīt programmu, kas veido pusstatiskās datu struktūras (MaxSize
– 10) un izpilda šādu funkciju secību:
1) izveidot pusstatiskās datu struktūras (DS) atbilstoši variantam (sk. tabulu); kamēr DS nav izveidota,
citas funkcijas vai nu ir nepieejamas vai, izvēloties kādu no tām, lietotājam tiek izvadīts paziņojums,
ka DS nav izveidota; elementu skaitu ievada programmas lietotājs;
2) aizpildīt pusstatiskās datu struktūras ar veseliem elementiem un ievērojot DS pievienošanas
koncepciju (LIFO, FIFO), pievienot jaunu elementu:
    a) ar funkciju push;
3) ievērojot pusstatiskās DS dzēšanas koncepciju (LIFO, FIFO), dzēst no DS elementu:
    a) ar funkciju pop;
4) paredzēt iespēju funkciju Size, Peek, Empty un Full rezultātus izvadi;
5) izpildīt ar pusstatisko DS šādas darbības:
    a) atrast to elementu daudzumu, kas ir vienādi ar nulli;


Speciālās prasības programmai:
• programmēšanas valoda - pēc autora izvēles;
• programmā obligāti jārealizē:
- informācijas par autoru izvadi (vārds, uzvārds, grupa, apliecības numurs);
- pusstatiskās datu struktūras apstrāde atbilstoši variantam;
• lietotāja saskarnei ir jābūt latviešu valodā;
• programmā jāiekļauj lietotāja nepareizu darbību apstrāde, izvadot uz ekrāna atbilstošo paziņojumu par
kļūdu;
Prasības failu nosaukumiem:
• darbs jānodod elektroniskā formā ORTUS vidē;
• prasības failu nosaukumiem: Ld2_variantanumurs (piemēram, ja Jums ir 8 variants, tad jāraksta Ld2_8).