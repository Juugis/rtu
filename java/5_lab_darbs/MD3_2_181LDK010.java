import java.util.Random;

public class MD3_2_181LDK010
{
   public static void main(String args[]) {
      long laiks1, laiks2, laiki[] = new long[3];
      int pozicija = 0;

      // Informācija par autoru
      System.out.println("\nAutors: Jurģis Brūvers");
      System.out.println("Apliecības Nr: 181LDK010");
      System.out.println("Kurss: 2. Koledža RDKD0");
      System.out.println("MD 3: 2. variants");

      // Aizpilda 5x5 100x100 un 500x500 masīvus
      int masivs100[] = masAizpildisana(100);

      // Izvada 100 masīvu
      System.out.println("\nMasīvs 100");
      for (int i=1; i<masivs100.length; i++) {
         System.out.print(masivs100[i-1]+",");
         if(i % 10 == 0){
            System.out.println();
         }
      }


      // lineārās nesakārtota meklēšana ar robežmarķiera algoritmu
      System.out.print("\nLūdzu ievadiet meklējamo vērtību starp -100 un 100: ");
      int n = Integer.parseInt(System.console().readLine());
      System.out.println("\nJūs meklējāt skaitli: "+n);
      laiks1 = System.nanoTime();
      pozicija = atrastPirmoSakritibu(n,masivs100);
      laiks2 = System.nanoTime();
      laiki[0] = laiks2-laiks1;
      if(pozicija == -1){
         System.out.println("Ievadītais skaitlis neatrodas masīvā!");
      }else{
         System.out.println("Skaitlis tika atrasts: "+pozicija+" Pozīcijā.");
      }

      // Masīva sakārtošana un izvade
      int[] sortedMasivs100 = insertionSort(masivs100);
      // Izprintē 100 masīvu
      System.out.println("\nSakārtots Masīvs 100");
      for (int i=1; i<sortedMasivs100.length; i++) {
         System.out.print(sortedMasivs100[i-1]+",");
         if(i % 10 == 0){
            System.out.println();
         }
      }

      // BINĀRĀ Meklēšanas sakārtotā masīvā
      System.out.print("\nLūdzu ievadiet meklējamo vērtību starp -100 un 100: ");
      n = Integer.parseInt(System.console().readLine());
      System.out.println("\nJūs meklējāt skaitli: "+n);
      laiks1 = System.nanoTime();
      pozicija = binaraMeklesana(n,sortedMasivs100);
      laiks2 = System.nanoTime();
      laiki[1] = laiks2-laiks1;
      if(pozicija == -1){
         System.out.println("Ievadītais skaitlis neatrodas masīvā!");
      }else{
         System.out.println("Skaitlis tika atrasts ar Bināro meklēšanu: "+pozicija+" Pozīcijā.");
      }

      // Lineārā meklēšana sakārtotā masīvā
      laiks1 = System.nanoTime();
      pozicija = atrastPirmoSakritibu(n,sortedMasivs100);
      laiks2 = System.nanoTime();
      laiki[2] = laiks2-laiks1;
      if(pozicija == -1){
         System.out.println("Ievadītais skaitlis neatrodas masīvā!");
      }else{
         System.out.println("Skaitlis tika atrasts ar Lineāro meklēšanu: "+pozicija+" Pozīcijā.");
      }

      String[] masivi = new String[30];
      masivi[0] = "Nesakārtots \t||\t Lineārs     \t";
      masivi[1] = "Sakārtots \t||\t Binārs     \t";
      masivi[2] = "Sakārtots \t||\t Lineārs     \t";


      // Rezultāti
      System.out.println("\n==================================================================");
      System.out.println("||\tMasīva veids  \t||     Meklēšanas veids  || \tlaiks   ||");
      System.out.println("------------------------------------------------------------------");
      for (int i = 0; i < 3; i++) {
         System.out.print("||\t"+masivi[i]+"");
         System.out.print("|| \t "+laiki[i]+"\t");

         System.out.print("||");
         System.out.println("\n------------------------------------------------------------------");
      }
   }

   // Masīvu aizpildīšanas funkcija
   static int[] masAizpildisana(int a) {
      int masivs[] = new int[a];
      Random randNr = new Random();
      for (int i = 0; i < a; i++) {
         masivs[i] = randNr.nextInt(201) - 100;
      }
      return masivs;
   }

   static int atrastPirmoSakritibu(int find, int[] a) {
      int pozicija = -1;
      for (int i = 0; i < a.length; i++) {
         if(a[i] == find){
            pozicija = i;
            break;
         }
      }
      return pozicija;
   }

   static int binaraMeklesana(int key, int[] arr){
      int first = 0;
      int last = arr.length-1;
      int mid = (first + last)/2;

      while( first <= last ){
         if ( arr[mid] > key ){
           first = mid + 1;     
         }else if ( arr[mid] == key ){  
           //System.out.println("Element is found at index: " + mid);  
           break;  
         }else{  
            last = mid - 1;  
         }  
         mid = (first + last)/2;  
      }  

      if ( first > last ){  
         //System.out.println("Element is not found!"); 
         mid = -1; 
      }

      return mid;
   }

   static int[] insertionSort(int[] array) {
      for (int j = 1; j < array.length; j++) {  
         int key = array[j];  
         int i = j-1;  
         while ( (i > -1) && ( array [i] < key ) ) {  
               array [i+1] = array [i];  
               i--;  
         }  
         array[i+1] = key;  
      }
      return array;
   }
}