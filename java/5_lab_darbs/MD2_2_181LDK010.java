import java.util.Random;

public class MD2_2_181LDK010
{
   public static void main(String args[]) {
      int[][] rezultati = new int[3][2];
      long laiks1, laiks2, laiki[][] = new long[3][3];
      int rezSkaits;

      // Informācija par autoru
      System.out.println("\nAutors: Jurģis Brūvers");
      System.out.println("Apliecības Nr: 181LDK010");
      System.out.println("Kurss: 2. Koledža RDKD0");
      System.out.println("MD: 2. variants");

      // Aizpilda 5x5 100x100 un 500x500 masīvus
      int masivs5[][] = masAizpildisana(5);
      int masivs100[][] = masAizpildisana(100);
      int masivs500[][] = masAizpildisana(500);

      // Aizpilda 5x5 100x100 un 500x500 vektorus
      int[] v5 = masivsUzVectoru(masivs5);
      int[] v100 = masivsUzVectoru(masivs100);
      int[] v500 = masivsUzVectoru(masivs500);

      // Izprintē 5x5 masīvu
      System.out.println("\nMasīvs 5x5");
      for (int i=0; i<masivs5.length; i++) {
         for(int number : masivs5[i]) {
            System.out.print(number+",");
         }
         System.out.println();
      }
      // Izprintē 5x5 vektoru
      System.out.println("\nVektors 5x5");
      for (int i=0; i<v5.length; i++) {
         if(v5[i] != 0){
            System.out.print(v5[i]+",");
         }
      }
      System.out.println();

      // noteikt laikus saglabat laikus masiva un aprekinat starpibu
      laiks1 = System.nanoTime();
      rezSkaits = negParaSkaitsMas(masivs5);
      laiks2 = System.nanoTime();
      laiki[0][0] = laiks2-laiks1;
      laiks1 = System.nanoTime();
      rezSkaits = negParaSkaitsVec(v5);
      laiks2 = System.nanoTime();
      laiki[0][1] = laiks2-laiks1;
      laiki[0][2] = laiki[0][0] - laiki[0][1];
      rezultati[0][0] = 5;
      rezultati[0][1] = rezSkaits;

      laiks1 = System.nanoTime();
      rezSkaits = negParaSkaitsMas(masivs100);
      laiks2 = System.nanoTime();
      laiki[1][0] = laiks2-laiks1;
      laiks1 = System.nanoTime();
      rezSkaits = negParaSkaitsVec(v100);
      laiks2 = System.nanoTime();
      laiki[1][1] = laiks2-laiks1;
      laiki[1][2] = laiki[1][0] - laiki[1][1];
      rezultati[1][0] = 100;
      rezultati[1][1] = rezSkaits;

      laiks1 = System.nanoTime();
      rezSkaits = negParaSkaitsMas(masivs500);
      laiks2 = System.nanoTime();
      laiki[2][0] = laiks2-laiks1;
      laiks1 = System.nanoTime();
      rezSkaits = negParaSkaitsVec(v500);
      laiks2 = System.nanoTime();
      laiki[2][1] = laiks2-laiks1;
      laiki[2][2] = laiki[2][0] - laiki[2][1];
      rezultati[2][0] = 500;
      rezultati[2][1] = rezSkaits;


      System.out.println("\n===================================================================================");
      System.out.println("||\tizmērs ||  rezultāts  || laiks1(matrica)|| laiks2(vektors)||  starpība  ||");
      System.out.println("-----------------------------------------------------------------------------------");

      for (int i = 0; i < rezultati.length; i++) {
         for (int j = 0; j < rezultati[0].length; j++) {
            System.out.print("||\t"+rezultati[i][j]+"\t");
         }

         for (int j = 0; j < laiki[0].length; j++) {
            System.out.print("||  "+laiki[i][j]+"\t");
         }
         System.out.print("||");
         System.out.println("\n-----------------------------------------------------------------------------------");
      }
   }

   // Masīvu aizpildīšanas funkcija
   static int[][] masAizpildisana(int a) {
      int masivs[][] = new int[a][a];
      Random randNr = new Random();

      for (int i = 0; i < a; i++) {
         for (int j = 0; j < a; j++) {
            if(i>a-j-1){
               masivs[i][j] = 0;
            }else{
               masivs[i][j] = randNr.nextInt(20) - 10;
            }
         }
      }
      return masivs;
   }

   // Vector aizpildīšanas funkcija
   static int[] masivsUzVectoru(int a[][]) {
      int n = a.length;
      int k = 0;
      int[] v = new int[n * (n + 1) / 2];

      for (int i = 0; i < n; i++) {
         for (int j = 0; j < n; j++) {
            if(a[i][j] != 0){
               v[k] = a[i][j];
               k++;
            }
         }
      }
      return v;
   }

   static int negParaSkaitsMas(int[][] a) {
      int skaits = 0;
      for (int i = 0; i < a.length; i++) {
         for (int j = 0; j < a.length; j++) {
            if(a[i][j] < 0){
               if(a[i][j] % 2 == 0){
                  skaits++;
               }
            }
         }
      }
      return skaits;
   }

   static int negParaSkaitsVec(int[] v) {
      int skaits = 0;
      for (int i = 0; i < v.length; i++) {
         if(v[i] < 0){
            if(v[i] % 2 == 0){
               skaits++;
            }
         }
      }
      return skaits;
   }
}