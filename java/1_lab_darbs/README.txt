
// Divkāršsaistīts cirkulārs saraksts: 1, 2a, 3, 4, 5c

Uzdevums: Izstrādāt algoritmu un uzrakstīt programmu, kas veido saistīto sarakstu (MaxSize – 10) un
izpilda šādu funkciju secību:
1) izveidot sarakstu atbilstoši variantam (sk. tabulu); kamēr saraksts nav izveidots, citas funkcijas vai nu
ir nepieejamas vai, izvēloties kādu no tām, lietotājam tiek izvadīts paziņojums, ka saraksts nav
izveidots;
2) aizpildīt sarakstu ar veseliem elementiem, pievienojot elementus:
    a) saistīta sarakstā: sākumā (insert at begining) vai beigās (insert at end);
3) izmest no saraksta elementu ar uzdoto pozīciju pos (vērtību pos ievada programmas lietotājs);
4) paredzēt iespēju funkciju Size, Empty un Full rezultātus izvadi;
5) izpildīt ar sarakstu šādas darbības un izvadīt rezultātus:
    c) saskaitīt cik ir sarakstā negatīvus elementus;


Speciālās prasības programmai:
• programmēšanas valoda – pēc autora izvēles;
• programmā obligāti jārealizē:
- informācijas par autoru izvadi (vārds, uzvārds, grupa, apliecības numurs);
- saistīta saraksta apstrāde atbilstoši variantam;
• lietotāja saskarnei ir jābūt latviešu valodā;
• programmā jāiekļauj lietotāja nepareizu darbību apstrāde, izvadot uz ekrāna atbilstošo paziņojumu par
kļūdu;
Prasības failu nosaukumiem:
• darbs jānodod elektroniskā formā ORTUS vidē;
• prasības failu nosaukumiem: Ld1_variantanumurs (piemēram, ja Jums ir 8 variants, tad jāraksta Ld1_8).