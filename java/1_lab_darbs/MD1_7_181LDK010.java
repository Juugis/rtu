import java.util.Scanner;

public class MD1_7_181LDK010
{
    public static void main(String args[]) {

        // Informācija par autoru
        System.out.println("\nAutors: Jurģis Brūvers");
        System.out.println("Apliecības Nr: 181LDK010");
        System.out.println("Kurss: 3. Koledža RDKD0");
        System.out.println("MD 1: 7. variants");

        Scanner scan = new Scanner(System.in);

        /* Creating object of linkedList */
        linkedList list = new linkedList();         
        char ch;

        /*  Perform list operations  */
        do{
            System.out.println("\nDarbības ar sarakstu:");
            System.out.println("1. insert at begining");
            System.out.println("2. insert at end");
            System.out.println("3. delete at position");
            System.out.println("4. Vai saraksts ir tukš?");
            System.out.println("5. Saraksta izmērs?");
            System.out.println("6. Vai saraksts ir pilns?");
            System.out.println("7. Cik ir sarakstā negatīvus elementu?\n");
        
            int choice = scan.nextInt();
            switch (choice){
            case 1 :
                if(list.isFull()){
                    System.out.println("Saraksts ir pilns!");
                }else{
                    System.out.println("Lūdzu ievadiet vesalu skaitli:");
                    list.insertAtStart( scan.nextInt() ); 
                }
                break;                          
            case 2 : 
                if(list.isFull()){
                    System.out.println("Saraksts ir pilns!");
                }else{
                    System.out.println("Lūdzu ievadiet vesalu skaitli:");
                    list.insertAtEnd( scan.nextInt() );      
                }               
                break;                                      
            case 3 : 
                System.out.println("Ievadiet sarakstā esošu pozīciju:");
                int p = scan.nextInt() ;
                if (p < 1 || p > list.getSize() )
                    System.out.println("Pozīcija sarakstā netika atrasta!\n");
                else
                    list.deleteAtPos(p);
                break;     
            case 4 :
                if(list.isEmpty()){
                    System.out.println("Saraksts ir tukš!\n");
                }else{
                    System.out.println("Saraksts nav tukš!\n");
                }               
                break;        
            case 5 : 
                System.out.println("Saraksta izmērs: "+ list.getSize() +" \n");
                break;   
            case 6 : 
                if(list.isFull()){
                    System.out.println("Saraksts ir pilns!\n");
                }else{
                    System.out.println("Saraksts nav pilns!\n");
                }
                break;
            case 7 : 
                    System.out.print("Negatīvu vērtību skaits sarakstā: "+list.getTotalNegativeIntCount());
                break; 

            default : 
                System.out.println("Opcija: "+choice+" neeksistē! \n ");
                break;   
            }

            /*  Display List  */ 
            list.display();
            System.out.println("\nTurpināt? (y vai n) \n");
            ch = scan.next().charAt(0);                        
        } while (ch == 'Y'|| ch == 'y');   
        scan.close();
   }
}

class Node{
    protected int data;
    protected Node next, prev;

    /* Constructor */
    public Node(){
        next = null;
        prev = null;
        data = 0;
    }

    /* Constructor */
    public Node(int d, Node n, Node p){
        data = d;
        next = n;
        prev = p;
    }

    /* Function to set link to next node */
    public void setLinkNext(Node n){
        next = n;
    }

    /* Function to set link to previous node */
    public void setLinkPrev(Node p){
        prev = p;
    }    

    /* Funtion to get link to next node */
    public Node getLinkNext(){
        return next;
    }

    /* Function to get link to previous node */
    public Node getLinkPrev(){
        return prev;
    }
    
    /* Function to set data to node */
    public void setData(int d){
        data = d;
    }

    /* Function to get data from node */
    public int getData(){
        return data;
    }
}

/* Class linkedList */
class linkedList{
    protected Node start;
    protected Node end ;
    public int size;
    public int maxSize;

    /* Constructor */
    public linkedList(){
        start = null;
        end = null;
        size = 0;
        maxSize = 10;
    }

    /* Function to check if list is empty */
    public boolean isEmpty(){
        return start == null;
    }

    /* Function to check if list is full */
    public boolean isFull(){
        if(maxSize == size){
            return true;
        }
        return false;
    }
    
    /* Function to get size of list */
    public int getSize(){
        return size;
    }

    public int getTotalNegativeIntCount(){
        Node ptr = start;
        int negativeCounter = 0;
        if (size == 0){
            System.out.print("Saraksts ir tukš!\n");
            return 0;
        }

        // if list has one inserted value
        if (start.getLinkNext() == start){
            //System.out.print(start.getData()+ " <-> "+ptr.getData()+ "\n");
            if(start.getData() >= 0){
                return 0;
            }else{
                return 1;
            }
        }

        // FOR MULTIPLE LIST ITEMS
        if(start.getData() < 0){
            negativeCounter++;
        }
        ptr = start.getLinkNext();
        while (ptr.getLinkNext() != start){
            if(ptr.getData() < 0){
                negativeCounter++;
            }
            ptr = ptr.getLinkNext();
        }
        if(ptr.getData() < 0){
            negativeCounter++;
        }
        ptr = ptr.getLinkNext();
        return negativeCounter;
    }


    /* Function to insert element at begining */
    public void insertAtStart(int val){
        Node nptr = new Node(val, null, null);    
        if (start == null){
            nptr.setLinkNext(nptr);
            nptr.setLinkPrev(nptr);
            start = nptr;
            end = start;            
        }
        else{
            nptr.setLinkPrev(end);
            end.setLinkNext(nptr);
            start.setLinkPrev(nptr);
            nptr.setLinkNext(start);
            start = nptr;        
        }
        size++ ;
    }

    /*Function to insert element at end */
    public void insertAtEnd(int val){
        Node nptr = new Node(val, null, null);        
        if (start == null){
            nptr.setLinkNext(nptr);
            nptr.setLinkPrev(nptr);
            start = nptr;
            end = start;
        }
        else{
            nptr.setLinkPrev(end);
            end.setLinkNext(nptr);
            start.setLinkPrev(nptr);
            nptr.setLinkNext(start);
            end = nptr;    
        }
        size++;
    }

    /* Function to delete node at position  */
    public void deleteAtPos(int pos){
        if (pos == 1){
            if (size == 1){
                start = null;
                end = null;
                size = 0;
                return; 
            }
            start = start.getLinkNext();
            start.setLinkPrev(end);
            end.setLinkNext(start);
            size--; 
            return ;
        }
        if (pos == size){
            end = end.getLinkPrev();
            end.setLinkNext(start);
            start.setLinkPrev(end);
            size-- ;
        }
        Node ptr = start.getLinkNext();
        for (int i = 2; i <= size; i++){
            if (i == pos){
                Node p = ptr.getLinkPrev();
                Node n = ptr.getLinkNext();
                p.setLinkNext(n);
                n.setLinkPrev(p);
                size-- ;
                return;
            }
            ptr = ptr.getLinkNext();
        }        
    }    
    
    /* Function to display status of list */
    public void display(){
        Node ptr = start;
        if (size == 0){
            return;
        }

        System.out.print("\n Saraksts:");
        if (start.getLinkNext() == start){
            System.out.print(start.getData()+ " <-> "+ptr.getData()+ "\n");
            return;
        }
        System.out.print(start.getData()+ " <-> ");
        ptr = start.getLinkNext();
        while (ptr.getLinkNext() != start){
            System.out.print(ptr.getData()+ " <-> ");
            ptr = ptr.getLinkNext();
        }

        System.out.print(ptr.getData()+ " <-> ");
        ptr = ptr.getLinkNext();
        System.out.print(ptr.getData()+ "\n");
    }
}